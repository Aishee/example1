#include<iostream>
#include<fstream>
#include<string.h>
#include<math.h>

using namespace std;
struct pix
{
unsigned char b,g,r;
}
pixel;
int L=255;//L is set to highest intensity per byte
char Header[54];
float gamma1=0;
ifstream in;
ofstream out,out1;

int main()
{
 char infile[]="lena512.bmp";
 char outfile[]="lena512-mod.bmp";
 char imdata[]="1506732.dat";
 in.open(infile,ios::in|ios::binary);
 in.read((char*)(&Header),sizeof(Header));
 out.open(outfile,ios::out|ios::binary);
 out.write((char*)(&Header),sizeof(Header));
 out1.open(imdata,ios::out);
 cout<<"enter the value for power factor: ";
 cin>>gamma1;
 while(!in.eof())
  {
     in.read((char*) (&pixel),sizeof(pixel));
     out1<<"ORIGINAL : "<<(int)pixel.r<<" , "<<(int)pixel.g<<" , "<<(int)pixel.b<<endl;
     pixel.r=L*pow(pixel.r,gamma1);
     pixel.g=L*pow(pixel.g,gamma1);
     pixel.b=L*pow(pixel.b,gamma1);
     out.write((char*) (&pixel),sizeof(pixel));
     out1<<"MODIFIED : "<<(int)pixel.r<<" "<<(int)pixel.g<<" "<<(int)pixel.b<<endl;
  }
 in.close();
 out.close();
 out1.close();
}